# Rchelsa



## Introduction

The Rchelsa package provides functions to access, load, crop, and extract
CHELSA data in R. 

## Installation

Rchelsa is under development. Until release, you can only install it from 
gitlabext.wsl.ch. After release it is planned to make it available via 
CRAN as well.

Not all data is accessible yet!!!


To install the package you will need to have the R package devtools installed
```
install.packages("devtools")
library(devtools)
install_git("https://gitlabext.wsl.ch/karger/rchelsa.git")
```

## Usage
Rchelsa provides serveral functions that allow extracting data for given 
localities. The getChelsa() function wraps most functions of for the different
datasets.

### Examples

### extracting a daily climate timeseries for a given extent data 
from CHELSA V2.1

In this example we would like to extract temperature data for Switzerland
for the first half of 2023. For this we need to define the temporal
extent as dates and spatial extent in the form xmin, xmax, ymin, ymax first:

```R
extent <- c(5.3, 10.4, 46.0, 47.5)
startdate <- as.Date("2023-1-1")
enddate <- as.Date("2023-6-30")
```

Now we set these extents as parameter of the function:
```R
tas <- getChelsa('tas',extent=extent, startdate=startdate, enddate=enddate)
```
The result is a spatial raster with each layer representing a specific day.

### extracting point data from CHELSA V2.1

First set the coordinates you want to extract data from in the following form:
```R
coords <- data.frame(lon = -5.36878, lat = 36.75864)
```

Then you can run the getChelsa() function:
```R
tas <- getChelsa('tas',coords=coords, startdate=startdate, enddate=enddate)
```


### extracting point data from CHELSA_TraCE21k

In this example we extract data for a set of localities for the the entire
timeseries since the Last Glacial Maximum (LGM).

1. We load the package and define the timeframe by setting a startdate and an 
enddate given by a vector containing the startmonth, and start century. As
R does not support a date format that is compatible with the centennial date
format of CHELSA_TraCE21k, the dates need to be strictly supplied in the format
of a vector containing (month, century).

```R
# load the package
library(Rchelsa)¨

# set start and enddate
startdate <- c(1,-200)
enddate <- c(12,20)
```

2. Define a data frame for which a climatic variable will be extracted:

```R
coords <- data.frame(lon = c(13.827138,
                             13.781620,
                             13.931188,
                             13.860942,
                             14.087698,
                             13.995614,
                             14.542631,
                             14.604594,
                             15.120419,
                             14.244438,
                             14.44117,
                             14.101014), 
                     lat = c(46.311087,
                             46.340116,
                             46.294933,
                             46.284336,
                             46.361817,
                             46.335302,
                             45.982440,
                             46.425777,
                             45.301463,
                             45.4364008,
                             45.252109,
                             45.118882))
```


3. extract pr, tasmax, and tasmin from CHELSA_TraCE21k:

```R
pr <- getChelsa('pr',
                coords = coords, 
                startdate = startdate, 
                enddate = enddate, 
                verbose = T, version="CHELSA_TraCE21k", freq="centennial")
                    
tasmax <- getChelsa('tasmax',
                    coords = coords, 
                    startdate = startdate, 
                    enddate = enddate, 
                    verbose = T, version="CHELSA_TraCE21k", freq="centennial")

tasmin <- getChelsa('tasmin',
                    coords = coords, 
                    startdate = startdate, 
                    enddate = enddate, 
                    verbose = T, version="CHELSA_TraCE21k", freq="centennial")
```

## Authors and acknowledgment
Dirk N. Karger
Philipp Brun

## License
GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

## Project status
under development
