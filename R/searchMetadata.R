#' Function to search the metadata of a CHELSA parameter
#'
#' Reads CHELSA metadata from the data repository and allows searching
#' the metadata for a given string. 
#' @param searchstring character, a character string to search in the metadata
#' @param var character, variable short name
#' @param date Date object (optional), use if single day is 
#' requested, default = (\code{NULL})
#' @param coords dataframe object (optional), containing a row 
#' named "lon", and a row named "lat", and the respective 
#' coordinates. default = (\code{NULL}) 
#' @param extent vector, longitudinal and latitudinal extent for which the data
#' needs to be cropped in the form of xmin, xmax, 
#' ymin, ymax. default = (\code{NULL}) 
#' @param startdate Date object (optional), start date of the time period 
#' requested, default = (\code{NULL}) 
#' @param enddate Date object (optional), end date of the time period 
#' requested, default = (\code{NULL}) 
#' @param version character (optional), CHELSA version to be requested, 
#' default=(\code{"V2"}) 
#' @param freq character (optional), temporal frequency to be requested, 
#' default=(\code{"daily"}) 
#' 
#' @returns string
#'   
#' @export
#'
#' @examples
#' # create a date for which a raster is requested
#' date = as.Date('2023-6-16')
#' # search for the unit in the metadata
#' searchMetadata('unit', 'hurs', date=date)
#' # search for the variable name in the metadata
#' searchMetadata('name', 'hurs', date=date)
#' # search for the nodata value in the metadata
#' searchMetadata('NoData', 'hurs', date=date)
#' 
searchMetadata <- function(searchstring, var, date=NULL, coords=NULL, extent=NULL, 
                           startdate=NULL, enddate=NULL, version="CHELSA", 
                           freq="daily"){
  md <- Rchelsa::getMetadata(var, date=date, coords=coords, extent=extent, 
                    startdate=startdate, enddate=enddate, version=version, 
                    freq=freq, verbose=FALSE)
  out <- md[grep(searchstring, md)]
  return(out)
}
