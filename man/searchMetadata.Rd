% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/searchMetadata.R
\name{searchMetadata}
\alias{searchMetadata}
\title{Function to search the metadata of a CHELSA parameter}
\usage{
searchMetadata(
  searchstring,
  var,
  date = NULL,
  coords = NULL,
  extent = NULL,
  startdate = NULL,
  enddate = NULL,
  version = "CHELSA",
  freq = "daily"
)
}
\arguments{
\item{searchstring}{character, a character string to search in the metadata}

\item{var}{character, variable short name}

\item{date}{Date object (optional), use if single day is 
requested, default = (\code{NULL})}

\item{coords}{dataframe object (optional), containing a row 
named "lon", and a row named "lat", and the respective 
coordinates. default = (\code{NULL})}

\item{extent}{vector, longitudinal and latitudinal extent for which the data
needs to be cropped in the form of xmin, xmax, 
ymin, ymax. default = (\code{NULL})}

\item{startdate}{Date object (optional), start date of the time period 
requested, default = (\code{NULL})}

\item{enddate}{Date object (optional), end date of the time period 
requested, default = (\code{NULL})}

\item{version}{character (optional), CHELSA version to be requested, 
default=(\code{"V2"})}

\item{freq}{character (optional), temporal frequency to be requested, 
default=(\code{"daily"})}
}
\value{
string
}
\description{
Reads CHELSA metadata from the data repository and allows searching
the metadata for a given string.
}
\examples{
# create a date for which a raster is requested
date = as.Date('2023-6-16')
# search for the unit in the metadata
searchMetadata('unit', 'hurs', date=date)
# search for the variable name in the metadata
searchMetadata('name', 'hurs', date=date)
# search for the nodata value in the metadata
searchMetadata('NoData', 'hurs', date=date)

}
